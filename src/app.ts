import { Route } from './core/interfaces';
import express from 'express'
import mongoose from 'mongoose';
import hpp from 'hpp';
import morgan from 'morgan';
import helmet from 'helmet';
import cros from 'cors';
import { Logger } from './core/utils';
import { errorMiddleware } from './core/middleware';

class App{
  public app: express.Application;
  public port: string | number;
  public production: boolean;

  constructor(routes: Route[]){
    this.app = express();
    this.port = process.env.PORT || 5000;
    this.production = process.env.NODE_ENV =='production' ? true : false;
  
    this.connectToDatabase();
    this.initializeMiddleware();
    this.initializeRoutes(routes); //notes: route must be after middle ware

    //Erro middle ware must have run lastly
    this.initializeErrorMiddleware()
  }

  private initializeRoutes(routes: Route[]){
    routes.forEach(route => {
      this.app.use("/", route.router);
    });
  }

  public listen(){
    this.app.listen(this.port,()=>{
      Logger.info(`Server is listening on port ${this.port}`);
    })
  }

  private initializeMiddleware(){
    if(this.production){
      this.app.use(hpp());
      this.app.use(helmet());
      this.app.use(morgan('combined'));
      this.app.use(cros({
        origin: 'your.domain.com',
        credentials: true
      }));
    }else{
      this.app.use(morgan('dev'));
      this.app.use(cros({
        origin: true,
        credentials: true
      }));
    }
    
    //accept request from res.body
    this.app.use(express.json());
    this.app.use(express.urlencoded({extended: true}));
  }

  private initializeErrorMiddleware(){
    this.app.use(errorMiddleware);
  }

  //connect to db
  private connectToDatabase(){
   
    const connectionString = process.env.MONGODB_URI;
    if(!connectionString){
      Logger.info('Connection string is Invalid');
      return;
    }
    mongoose.connect(connectionString,{
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: true,
      useCreateIndex: true,
    }).catch((reason)=>{
      Logger.error(reason);
    });
    Logger.info('connect success');
  }
}

export default App;