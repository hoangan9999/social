import jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from "express";
import { DataStoreInToken } from '@modules/auth';

const authMiddleware = (req: Request, res: Response, next: NextFunction) =>{

  const token = req.header('an-auth-token');

  if(!token){
    return res.status(401).json({message: 'No token, authorization denied'});
  }
  try {

    const user = jwt.verify(
      token,
      process.env.JWT_TOKEN_SECRET!
    ) as DataStoreInToken

    if(!req.user){
      req.user = { id : ''}
    }

    req.user.id = user.id; // request not exits user, we define it in types and config in ts config
    next() // if success must to next for handle step
  } catch (error) {
    res.status(401).json({message: 'Token is not valid'});
  }
}

export default authMiddleware;