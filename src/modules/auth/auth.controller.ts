import { TokenData } from "@modules/auth";
import { NextFunction, Request, Response } from "express";
import LoginDto from "./auth.dto";
import AuthService from "./auth.service";

export default class AuthController {

  private authService = new AuthService();

  public login = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const model: LoginDto = req.body;
      const TokenData: TokenData = await this.authService.login(model);
      res.status(200).json(TokenData); //200 login thành công
    } catch (error) {
      next(error);
    }
  }

  public getCurrentLoginUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = await this.authService.getCurrenLoginUser(req.user.id);
      res.status(200).json(user); 
    } catch (error) {
      next(error);
    }
  }

}