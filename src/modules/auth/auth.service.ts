import UserSchema from './../users/users.model';
import { DataStoreInToken, TokenData } from '@modules/auth';
import { HttpException } from '@core/exceptions';
import {isEmptyObject} from './../../core/utils';
import gravatar from 'gravatar';
import bcryptjs from 'bcryptjs';
import jwt from 'jsonwebtoken';
import LoginDto from './auth.dto';
import IUser from '@modules/users/users.interface';

class AuthService{
  public userSchema = UserSchema;

  public async login(model: LoginDto): Promise<TokenData>{
    if(isEmptyObject(model)){
      throw new HttpException(400, "Model is empty");
    }
    const user = await this.userSchema.findOne({email: model.email});
    if(!user){
      throw new HttpException(409, `Your email ${model.email} isn't exist.`);
    }

    const avatar = gravatar.url(model.email!,{
      size: '200',
      rating: 'g',
      default: 'mm'
    });

    const isMatchPassword = await bcryptjs.compare(model.password, user.password);
    if(!isMatchPassword){
      throw new HttpException(400, 'Credential is not valid');
    }

    return this.createToken(user);
  } 

  public async getCurrenLoginUser(userId: string): Promise<IUser>{
    const user = await this.userSchema.findById(userId);
    if(!user){
      throw new HttpException(404, `User is not exits`);
    }
    return user;
  } 

  private createToken(user: IUser): TokenData{
    const dataInToken: DataStoreInToken = {id: user._id};
    const secret: string = process.env.JWT_TOKEN_SECRET!;
    const expiresIn: number = 60;
    return {
      token : jwt.sign(dataInToken, secret, {expiresIn: expiresIn})
    }
  }
}

export default AuthService;