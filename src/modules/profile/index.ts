import ProfileSchema from "./profile.model";
import ProfileRoute from "./profile.route";
import {IProfile} from './profile.interface';

export {ProfileRoute, ProfileSchema, IProfile }