import { IUser } from "@modules/auth";
import { NextFunction, request, Request, RequestHandler, Response } from "express";
import AddExperienceDto from "./dtos/add_experience.dto";
import CreateProfileDto from "./dtos/create_profile.dto";
import { IProfile } from "./profile.interface";
import ProfileService from "./profile.service";

export default class ProfileController{
  private profileService = new ProfileService();

  public getCurrentProfile = async(req: Request, res: Response, next: NextFunction)=>{
    try {
      const userId = req.user.id;
      const resultObj: Partial<IUser> = await this.profileService.getCurrentProFile(
        userId
      );
      res.status(200).json(resultObj);

    } catch (error) {
      next(error);
    }
  }

  public getByUserId = async(req: Request, res: Response, next: NextFunction)=>{
    try {
      const userId = req.params.id;
      const resultObj: Partial<IUser> = await this.profileService.getCurrentProFile(
        userId
      );
      res.status(200).json(resultObj);

    } catch (error) {
      next(error);
    }
  }


  public getAllProfile = async(req: Request, res: Response, next: NextFunction)=>{
    try {
      const resultObj: Partial<IUser>[] = await this.profileService.getAllProFile();
      res.status(200).json(resultObj);

    } catch (error) {
      next(error);
    }
  }

  public createProfile = async(req: Request, res: Response, next: NextFunction)=>{
    try {
      const userId = req.user.id;
      const userData: CreateProfileDto = req.body;
      const createUserSchema: IProfile = await this.profileService.createProFile(userId, userData);
      res.status(200).json(createUserSchema);

    } catch (error) {
      next(error);
    }
  }

  public deleteProfile = async(req: Request, res: Response, next: NextFunction)=>{
    try {
      const userId = req.params.id;
      await this.profileService.deleteProfile(
        userId
      );
      res.status(200);

    } catch (error) {
      next(error);
    }
  }

  public createExperience = async (req: Request, res: Response, next: NextFunction) =>{
    const data: AddExperienceDto = req.body;
    const userId = req.body.userId;
    try {
      const createUserData: IProfile = await this.profileService.addExperience(userId, data);
      res.status(201).json(createUserData);
    } catch (error) {
      next(error)
    }
  }

  public deleteExperience = async (req: Request, res: Response, next: NextFunction) =>{
    const expId: string = req.params.exp_id;
    try {
      const profile = await this.profileService.deleteExperience(req.user.id, expId);
      res.status(200).json(profile);
    } catch (error) {
      next(error)
    }
  }

  

}