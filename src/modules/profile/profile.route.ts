import { Route } from "@core/interfaces";
import { authMiddleware, validationMiddleware } from "@core/middleware";
import { Router } from "express";
import AddExperienceDto from "./dtos/add_experience.dto";
import CreateProfileDto from "./dtos/create_profile.dto";
import ProfileController from "./profile.controller";

export default class ProfileRoute implements Route{
  public path = '/api/v1/profile';
  public router = Router();
  public profileControler = new ProfileController();

  constructor(){
    this.initializeRoutes();
  }

  private initializeRoutes(){

    this.router.get(`${this.path}`,this.profileControler.getAllProfile);

    this.router.get(`${this.path}/user/:id`, this.profileControler.getByUserId);

    this.router.get(`${this.path}/me`, authMiddleware, this.profileControler.getCurrentProfile);

    this.router.post(`${this.path}`, authMiddleware, validationMiddleware(CreateProfileDto), this.profileControler.createProfile);

    this.router.delete(`${this.path}/:id`, authMiddleware, this.profileControler.deleteProfile);

    this.router.put(`${this.path}/experience`, authMiddleware, validationMiddleware(AddExperienceDto), this.profileControler.createExperience);

    this.router.delete(`${this.path}/experience/:exp_id`, authMiddleware, this.profileControler.deleteExperience);
  }

}