import { HttpException } from "@core/exceptions";
import { IUser } from "@modules/auth";
import { IExperience, IProfile, ISocial } from "./profile.interface";
import ProfileSchema from './profile.model';
import normalize from 'normalize-url';
import CreateProfileDto from "./dtos/create_profile.dto";
import { UserSchema } from "@modules/users";
import AddExperienceDto from "./dtos/add_experience.dto";
import { Http } from "winston/lib/winston/transports";

class ProfileService{

  public async getCurrentProFile(userId: string): Promise<Partial<IUser>>{
    const user = await ProfileSchema.findOne({
      user: userId
    }).populate("user",["name","avatar"]);

    if(!user){
      throw new HttpException(400, 'There is no profile for this user');
    }
    return user;

  }

  public async createProFile(userId: string, profileDto: CreateProfileDto): Promise<IProfile>{
    const {
      company,
      location,
      website,
      bio,
      skills,
      status,
      youtube,
      twitter,
      instagram,
      linkedin,
      facebook
    } = profileDto

    const profileFields: Partial<IProfile> = {
      user: userId,
      company,
      location,
      website: website && website != "" ? normalize(website.toString(),{forceHttp: true}): "",
      bio,
      skills: Array.isArray(skills) ? skills : skills.split(",").map((skill: string)=>" " + skill.trim()),
      status
    }

    const socialFiles: ISocial = {
      youtube,
      twitter,
      instagram,
      linkedin,
      facebook
    }

    for(const [key,value] of Object.entries(socialFiles)){
      if(value && value.length > 0){
        socialFiles[key] = normalize(value, {forceHttps: true});
      }
    }

    profileFields.social = socialFiles;

    const profile = await ProfileSchema.findOneAndUpdate(
      {user: userId},
      {$set: profileFields},
      {new: true, upsert: true, setDefaultsOnInsert: true}
    )
    return profile;
  }

  public async deleteProfile(userId: string){
    await ProfileSchema.findOneAndRemove({user: userId})
    await UserSchema.findOneAndRemove({_id: userId})
  }

  public async getAllProFile(): Promise<Partial<IUser>[]>{
    const profiles = await ProfileSchema.find()
      .populate("user",["name","avatar"]);
    return profiles;
  }

  public addExperience = async (userId: string, experience: AddExperienceDto) =>{
    const newEx = {
      ...experience
    } 

    const profile = await ProfileSchema.findOne({ user: userId }).exec();

    if(!profile){
      throw new HttpException(400, 'There is not profile for this user');
    }

    profile.experience.unshift(newEx as IExperience);
    await profile.save();
    return profile;
  }

  public deleteExperience = async (userId: string, experienceId: string) => {
    
    const profile = await ProfileSchema.findOne({ user: userId }).exec();

    if(!profile){
      throw new HttpException(400, 'There is not profile for this user');
    }

    profile.experience = profile.experience.filter(
      (exp) => exp._id.toString() !== experienceId
    );

    await profile.save();
    return profile;

  }

}

export default ProfileService;