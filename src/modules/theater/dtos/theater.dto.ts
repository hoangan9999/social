import { IsNotEmpty, MinLength } from 'class-validator';
export default class TheaterDto{

  @IsNotEmpty({message: 'name cannot empty'})
  @MinLength(3,{message: 'name is too short, at least 3 character'})
  public name: string;
  public description: string;

  constructor(name: string, description: string){
    this.name = name;
    this.description = description;
  }
}