import TheaterSchema from "./theaters.model";
import TheatersRoute from "./theater.route";
import ITheater from "./theaters.interface";

export {TheatersRoute, TheaterSchema, ITheater }