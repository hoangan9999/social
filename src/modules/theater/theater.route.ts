import { Route } from "./../../core/interfaces";
import { Router } from "express";
import TheatersController from "./theaters.controller";
import TheaterDto from "./dtos/theater.dto";
import validationMiddleware from "@core/middleware/validation.middleware";

import { authMiddleware } from "@core/middleware";

export default class TheatersRoute implements Route {
  public path = '/api/theaters';
  public router = Router();

  public theatersController = new TheatersController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(
      this.path,
      validationMiddleware(TheaterDto, true),
      this.theatersController.CreateTheater);  // POST: http://localhost:5000/api/theaters

    this.router.put(
      this.path + '/:id',
      //authMiddleware,
      validationMiddleware(TheaterDto, true),
      this.theatersController.UpdateTheater);

    this.router.get(
      this.path + '/:id',
      this.theatersController.GetTheaterById);

    this.router.get(
      this.path,
      this.theatersController.GetAll);

    this.router.get(
      this.path + '/paging/:page',
      this.theatersController.GetAllPaging);


    this.router.delete(
      this.path + '/:id',
      //authMiddleware,
      this.theatersController.DeleteTheater
    );
  }

}