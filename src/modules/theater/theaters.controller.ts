import { IUser, TokenData } from "@modules/auth";
import { NextFunction, Request, Response } from "express";
import TheaterDto from "./dtos/theater.dto";
import TheaterService from "./theaters.service";

export default class TheatersController{

  private theaterService = new TheaterService();

  public CreateTheater = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const model: TheaterDto = req.body;
     await this.theaterService.CreateTheater(model);
     res.status(201).json(model); //201 tạo thành công
    } catch (error) {
      next(error);
    }
  }

  public GetTheaterById = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const theater = await this.theaterService.GetTheaterById(req.params.id);
     res.status(200).json(theater); 
    } catch (error) {
      next(error);
    }
  }

  public UpdateTheater = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const model: TheaterDto = req.body;
     const theater = await this.theaterService.UpdateTheater(req.params.id ,model);
     res.status(209).json(theater); 
    } catch (error) {
      next(error);
    }
  }


  public GetAll = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const theaters = await this.theaterService.GetAll();
     res.status(209).json(theaters); 
    } catch (error) {
      next(error);
    }
  }

  public GetAllPaging = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const page: number = Number(req.params.page);
     const keyword = req.query.keyword || '';

     const paginationResult = await this.theaterService.GetAllPaging(keyword.toString(), page);
     res.status(209).json(paginationResult); 
    } catch (error) {
      next(error);
    }
  }

  public DeleteTheater = async (req: Request, res: Response, next: NextFunction) =>{
    try {
      const result = await this.theaterService.DeleteTheater(req.params.id);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  }

}