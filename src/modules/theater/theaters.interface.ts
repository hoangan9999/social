export default interface ITheater{
  _id: string;
  name: string;
  description : string;
  date: Date; 
}