import mongoose from 'mongoose';
import ITheater from './theaters.interface';

const TheaterSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description:{
    type: String,
    required: true,
  },
  date:{
    type: Date,
    default: Date.now,
  }
});

export default mongoose.model<ITheater & mongoose.Document>("theater", TheaterSchema);