import TheaterSchema from './theaters.model';
import { TicketSchema } from '@modules/ticket';
import TheaterDto from './dtos/theater.dto';
import { DataStoreInToken, TokenData } from '@modules/auth';
import { HttpException } from '@core/exceptions';
import {isEmptyObject} from './../../core/utils';
import ITheater from './theaters.interface';
import jwt from 'jsonwebtoken';
import { IPagination } from '@core/interfaces';

class TheaterService{
  public theaterSchema = TheaterSchema;
  public ticketSchema = TicketSchema;

  public async CreateTheater(model: TheaterDto){
    if(isEmptyObject(model)){
      throw new HttpException(400, "Model is empty");
    }
    const theater = await this.theaterSchema.findOne({name: model.name});
    if(theater){
      throw new HttpException(409, `${model.name} already exist.`);
    }

    const createTheater: ITheater = await this.theaterSchema.create({
      ...model,
      date: Date.now()
    });

    return createTheater;
  } 


  public async UpdateTheater( theaterId: string, model: TheaterDto): Promise<ITheater>{
    if(isEmptyObject(model)){
      throw new HttpException(400, "Model is empty");
    }
    const theater = await this.theaterSchema.findById(theaterId);
    if(!theater){
      throw new HttpException(400, `Theater Id is not exsits`);
    }

    const theaterNameExist = await this.theaterSchema.findOne({name: model.name});

    

    if(theaterNameExist && theater.id !== theaterNameExist.id){
      throw new HttpException(409, `${model.name} already exist.`);
    }
    
    let updateTheaterById;

    updateTheaterById = await this.theaterSchema.findByIdAndUpdate(theaterId, {
      ...model,
    },{new: true});

    if(!updateTheaterById){
      throw new HttpException(409, 'You are not an user');
    }
   
    return updateTheaterById;
  } 


  public async GetTheaterById(theaterId: string): Promise<ITheater>{
    const theater = await this.theaterSchema.findById(theaterId);
    if(!theater){
      throw new HttpException(404, `Theater is not exits`);
    }
    return theater;
  }

  
  public async GetAll(): Promise<ITheater[]>{
    const theater = await this.theaterSchema.find();
    return theater;
  } 

  public async GetAllPaging(keyword: string, page: number): Promise<IPagination<ITheater>>{

    const pageSize: number = Number(process.env.PAGE_SIZE) || 10;

    let query = {};

    if(keyword){
      query = {$or:[{ name: keyword},{ description: keyword}]}
    }

    const theater = await this.theaterSchema.find(query).skip((page - 1) * pageSize).limit(pageSize).exec();

    const rowCount = await this.theaterSchema.find(query).countDocuments().exec();  // estimatedDocumentCount same countDocuments but it faster

    return {
      total: rowCount,
      page: page,
      pageSize: pageSize,
      items: theater
    } as IPagination<ITheater>;

  } 

  public async DeleteTheater(theaterId: string): Promise<ITheater>{

    const ticketExistInTheater = await this.ticketSchema.find({$or:[{ theaterId: theaterId}]});
    if(ticketExistInTheater.length > 0){
      throw new HttpException(409, 'already ticket in this theater, please delete all ticket and try again');
    }

    const deletedTheater = await this.theaterSchema.findByIdAndDelete(theaterId);
    if(deletedTheater == null){
      throw new HttpException(409, 'Id not valid');
    }
    return deletedTheater
  }
}

export default TheaterService;