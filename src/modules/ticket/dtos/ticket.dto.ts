import { IsEmail, IsNotEmpty, MinLength, IsBoolean } from 'class-validator';
export default class TicketDto{

  @IsNotEmpty({message: 'name cannot empty'})
  @MinLength(3,{message: 'name is too short, at least 3 character'})
  public name: string;
  public description: string;
  @IsBoolean()
  public isSold: boolean;
  @IsNotEmpty({message: 'theaterId cannot empty'})
  public theaterId: string;

  constructor(name: string, description: string, isSold: boolean, theaterId: string){
    this.name = name;
    this.description = description;
    this.isSold = isSold;
    this.theaterId = theaterId;
  }
}