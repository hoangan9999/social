import TicketSchema from "./tickets.model";
import TicketsRoute from "./ticket.route";
import ITicket from "./tickets.interface";

export {TicketsRoute, TicketSchema, ITicket }