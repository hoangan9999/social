import { Route } from "../../core/interfaces";
import { Router } from "express";
import TicketsController from "./tickets.controller";
import TicketDto from "./dtos/ticket.dto";
import validationMiddleware from "@core/middleware/validation.middleware";

import { authMiddleware } from "@core/middleware";

export default class TicketsRoute implements Route {
  public path = '/api/tickets';
  public router = Router();

  public ticketsController = new TicketsController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(
      this.path,
      validationMiddleware(TicketDto, true),
      this.ticketsController.CreateTicket);  // POST: http://localhost:5000/api/tickets

    this.router.put(
      this.path + '/:id',
      //authMiddleware,
      validationMiddleware(TicketDto, true),
      this.ticketsController.UpdateTicket);

    this.router.get(
      this.path + '/:id',
      this.ticketsController.GetTicketById);

    this.router.get(
      this.path,
      this.ticketsController.GetAll);

    this.router.get(
      this.path + '/paging/:page',
      this.ticketsController.GetAllPaging);

    this.router.get(
      this.path + '/paging/:theaterId/:page',
      this.ticketsController.GetAllTicketByTheaterPaging);

    this.router.delete(
      this.path + '/:id',
      //authMiddleware,
      this.ticketsController.DeleteTicket
    );
  }

}