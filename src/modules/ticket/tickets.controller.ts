import { IUser, TokenData } from "@modules/auth";
import { NextFunction, Request, Response } from "express";
import TicketDto from "./dtos/ticket.dto";
import TicketService from "./tickets.service";

export default class TicketsController{

  private ticketService = new TicketService();

  public CreateTicket = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const model: TicketDto = req.body;
     await this.ticketService.CreateTicket(model);
     res.status(201).json(model); //201 tạo thành công
    } catch (error) {
      next(error);
    }
  }

  public GetTicketById = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const theater = await this.ticketService.GetTicketById(req.params.id);
     res.status(200).json(theater); 
    } catch (error) {
      next(error);
    }
  }

  public UpdateTicket = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const model: TicketDto = req.body;
     const ticket = await this.ticketService.UpdateTicket(req.params.id ,model);
     res.status(209).json(ticket); 
    } catch (error) {
      next(error);
    }
  }


  public GetAll = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const tickets = await this.ticketService.GetAll();
     res.status(209).json(tickets); 
    } catch (error) {
      next(error);
    }
  }

  public GetAllPaging = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const page: number = Number(req.params.page);
     const keyword = req.query.keyword || '';

     const paginationResult = await this.ticketService.GetAllPaging(keyword.toString(), page);
     res.status(209).json(paginationResult); 
    } catch (error) {
      next(error);
    }
  }

  public GetAllTicketByTheaterPaging = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const page: number = Number(req.params.page);
     const theaterId = req.params.theaterId;

     const paginationResult = await this.ticketService.GetAllTicketByTheaterPaging(theaterId, page);
     res.status(209).json(paginationResult); 
    } catch (error) {
      next(error);
    }
  }

  public DeleteTicket = async (req: Request, res: Response, next: NextFunction) =>{
    try {
      const result = await this.ticketService.DeleteTicket(req.params.id);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  }

}