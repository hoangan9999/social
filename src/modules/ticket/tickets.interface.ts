export default interface ITicket{
  _id: string;
  name: string;
  description: string;
  isSold: boolean;
  theaterId: string;
  theaterName: string;
  date: Date; 
}