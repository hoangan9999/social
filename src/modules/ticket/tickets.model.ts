import mongoose from 'mongoose';
import ITicket from './tickets.interface';

const TicketSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  isSold: {
    type: Boolean,
    require: true
  },
  theaterId:{
    type: String,
    require: true
  },
  theaterName: {
    type: String,
    require: true
  },
  date:{
    type: Date,
    default: Date.now,
  }
});

export default mongoose.model<ITicket & mongoose.Document>("ticket", TicketSchema);