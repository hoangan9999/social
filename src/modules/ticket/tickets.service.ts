import TicketSchema from './tickets.model';
import { TheaterSchema } from '@modules/theater';
import TicketDto from './dtos/ticket.dto';
import { DataStoreInToken, TokenData } from '@modules/auth';
import { HttpException } from '@core/exceptions';
import {isEmptyObject} from './../../core/utils';
import ITicket from './tickets.interface';
import jwt from 'jsonwebtoken';
import { IPagination } from '@core/interfaces';

class TicketService{
  public ticketSchema = TicketSchema;
  public theaterSchema = TheaterSchema;

  public async CreateTicket(model: TicketDto){
    if(isEmptyObject(model)){
      throw new HttpException(400, "Model is empty");
    }
    const ticket = await this.ticketSchema.findOne({name: model.name, theaterId: model.theaterId});
    if(ticket){
      throw new HttpException(409, `${model.name} already exist in this theater.`);
    }
    const theater = await this.theaterSchema.findById(model.theaterId);
    if(!theater){
      throw new HttpException(404, `TheaterID is not exits`);
    }
    const createTicket: ITicket = await this.ticketSchema.create({
      ...model,
      date: Date.now()
    });

    return createTicket;
  } 


  public async UpdateTicket( ticketId: string, model: TicketDto): Promise<ITicket>{
    if(isEmptyObject(model)){
      throw new HttpException(400, "Model is empty");
    }
    const ticket = await this.ticketSchema.findById(ticketId);
    if(!ticket){
      throw new HttpException(400, `Ticket Id is not exsits`);
    }

    if(ticket.theaterId !== model.theaterId){
      throw new HttpException(400, `Cannot move ticket into another theater`);
    }

    const ticketNameExist = await this.ticketSchema.findOne({name: model.name, theaterId: ticket.theaterId});

    if(ticketNameExist && ticket.id !== ticketNameExist.id){
      throw new HttpException(409, `${model.name} already exist in this theater.`);
    }
    
    let updateTicketById;

    updateTicketById = await this.ticketSchema.findByIdAndUpdate(ticketId, {
      ...model,
    },{new: true});

    if(!updateTicketById){
      throw new HttpException(409, 'Cannot update, please try again!');
    }
   
    return updateTicketById;
  }

  public async GetTicketById(ticketId: string): Promise<ITicket>{
    const ticket = await this.ticketSchema.findById(ticketId);
    if(!ticket){
      throw new HttpException(404, `Ticket is not exits`);
    }
    return ticket;
  }

  
  public async GetAll(): Promise<ITicket[]>{
    const ticket = await this.ticketSchema.find();
    return ticket;
  } 

  public async GetAllPaging(keyword: string, page: number): Promise<IPagination<ITicket>>{

    const pageSize: number = Number(process.env.PAGE_SIZE) || 10;

    let query = {};

    if(keyword){
      query = {$or:[{ name: keyword},{ description: keyword}]}
    }

    const ticket = await this.ticketSchema.find(query).skip((page - 1) * pageSize).limit(pageSize).exec();

    const rowCount = await this.ticketSchema.find(query).countDocuments().exec();  // estimatedDocumentCount same countDocuments but it faster

    return {
      total: rowCount,
      page: page,
      pageSize: pageSize,
      items: ticket
    } as IPagination<ITicket>;

  } 

  public async GetAllTicketByTheaterPaging(theaterId: string, page: number): Promise<IPagination<ITicket>>{

    const pageSize: number = Number(process.env.PAGE_SIZE) || 10;

    let query = {$or:[{ theaterId: theaterId}]}
    
    const ticket = await this.ticketSchema.find(query).skip((page - 1) * pageSize).limit(pageSize).exec();

    const rowCount = await this.ticketSchema.find(query).countDocuments().exec();  // estimatedDocumentCount same countDocuments but it faster

    return {
      total: rowCount,
      page: page,
      pageSize: pageSize,
      items: ticket
    } as IPagination<ITicket>;

  } 

  public async DeleteTicket(ticketId: string): Promise<ITicket>{
    const deletedTicket = await this.ticketSchema.findByIdAndDelete(ticketId);
    if(deletedTicket == null){
      throw new HttpException(409, 'Id not valid');
    }
    return deletedTicket;
  }
}

export default TicketService;