import { IUser, TokenData } from "@modules/auth";
import { NextFunction, Request, Response } from "express";
import RegisterDto from "./dtos/register.dto";
import UserService from "./users.service";

export default class UsersController{

  private userService = new UserService();

  public register = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const model: RegisterDto = req.body;
     const TokenData : TokenData = await this.userService.createUser(model);
     res.status(201).json(TokenData); //201 tạo thành công
    } catch (error) {
      next(error);
    }
  }

  public getUserById = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const user = await this.userService.getUserById( req.params.id);
     res.status(200).json(user); 
    } catch (error) {
      next(error);
    }
  }

  public updateUser = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const model: RegisterDto = req.body;
     const user = await this.userService.UpdateUser(req.params.id ,model);
     res.status(209).json(user); 
    } catch (error) {
      next(error);
    }
  }


  public getAll = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const users = await this.userService.getAll();
     res.status(209).json(users); 
    } catch (error) {
      next(error);
    }
  }

  public getAllPaging = async (req: Request, res: Response, next: NextFunction) =>{
    try {
     const page: number = Number(req.params.page);
     const keyword = req.query.keyword || '';

     const paginationResult = await this.userService.getAllPaging(keyword.toString(), page);
     res.status(209).json(paginationResult); 
    } catch (error) {
      next(error);
    }
  }

  public deleteUser = async (req: Request, res: Response, next: NextFunction) =>{
    try {
      const result = await this.userService.deleteUser(req.params.id);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  }

}