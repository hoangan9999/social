import mongoose from 'mongoose';
import IUser from './users.interface';

const UserSchema = new mongoose.Schema({
  first_name: {
    type: String,
    required: true,
  },
  last_name:{
    type: String,
    required: true,
  },
  email:{
    type: String,
    unique: true, // là duy nhất
    index: true, // có index để query cho nhanh
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  avatar: {
    type: String
  },
  date:{
    type: Date,
    default: Date.now,
  }
});

// tạo ra bảng user với những cái define của userSchema với strong type là IUser sẽ map với Moongose.document
// trong mongoose.document có _id nên ko cần khai báo id, thằng _id trong IUser sẽ hứng lại id,
export default mongoose.model<IUser & mongoose.Document>("user", UserSchema);