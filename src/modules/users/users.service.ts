import UserSchema from './users.model';
import RegisterDto from './dtos/register.dto';
import { DataStoreInToken, TokenData } from '@modules/auth';
import { HttpException } from '@core/exceptions';
import {isEmptyObject} from './../../core/utils';
import gravatar from 'gravatar';
import bcryptjs from 'bcryptjs';
import IUser from './users.interface';
import jwt from 'jsonwebtoken';
import { IPagination } from '@core/interfaces';

class UserService{
  public userSchema = UserSchema;

  public async createUser(model: RegisterDto): Promise<TokenData>{
    if(isEmptyObject(model)){
      throw new HttpException(400, "Model is empty");
    }
    const user = await this.userSchema.findOne({email: model.email});
    if(user){
      throw new HttpException(409, `Your email ${model.email} already exist.`);
    }

    const avatar = gravatar.url(model.email!,{
      size: '200',
      rating: 'g',
      default: 'mm'
    });

    const salt = await bcryptjs.genSalt(10); //hàm trả về promise nên await để lấy đc giá trị của nó
    const hashedPassword = await bcryptjs.hash(model.password!, salt);

    const createUser: IUser = await this.userSchema.create({
      ...model,
      password: hashedPassword,
      avatar: avatar,
      date: Date.now()
    });

    return this.createToken(createUser);
  } 


  public async UpdateUser( userId: string, model: RegisterDto): Promise<IUser>{
    if(isEmptyObject(model)){
      throw new HttpException(400, "Model is empty");
    }
    const user = await this.userSchema.findById(userId);
    if(!user){
      throw new HttpException(400, `User Id is not exsits`);
    }

    let avatar = user.avatar;
    if(user.email === model.email){
      throw new HttpException(400, `You must using the differnce Email`);
    }

    const isExistEmail = await this.userSchema.find({
      $and: [{ email: { $eq: model.email } }, { _id: {$ne: userId } }]
    });
    
    if(isExistEmail.length !== 0){
      throw new HttpException(400, 'your email has been used by another user');
      
    }

    avatar = gravatar.url(model.email!,{
      size: '200',
      rating: 'g',
      default: 'mm'
    });
    

    let updateUserById;

    if(model.password){
      const salt = await bcryptjs.genSalt(10);
      const hashedPassword = await bcryptjs.hash(model.password, salt);

      updateUserById = await this.userSchema.findByIdAndUpdate(userId, {
        ...model,
        avatar: avatar,
        password: hashedPassword
      },{new: true}); // new = true it mean result = news user update
    }
    else{
      updateUserById = await this.userSchema.findByIdAndUpdate(userId, {
        ...model,
        avatar: avatar,
      },{new: true});
    }

    if(!updateUserById){
      throw new HttpException(409, 'You are not an user');
    }
   
    return updateUserById;
  
  } 


  public async getUserById(userId: string): Promise<IUser>{
    const user = await this.userSchema.findById(userId);
    if(!user){
      throw new HttpException(404, `User is not exits`);
    }
    return user;
  }

  
  public async getAll(): Promise<IUser[]>{
    const user = await this.userSchema.find();
    return user;
  } 

  public async getAllPaging(keyword: string, page: number): Promise<IPagination<IUser>>{

    const pageSize: number = Number(process.env.PAGE_SIZE) || 10;

    let query = {};

    if(keyword){
      query = {$or:[{email: keyword},{ first_name: keyword},{last_name: keyword}]}
    }

    const user = await this.userSchema.find(query).skip((page - 1) * pageSize).limit(pageSize).exec();

    const rowCount = await this.userSchema.find(query).countDocuments().exec();  // estimatedDocumentCount same countDocuments but it faster

    return {
      total: rowCount,
      page: page,
      pageSize: pageSize,
      items: user
    } as IPagination<IUser>;

  } 

  public async deleteUser(userId: string): Promise<IUser>{
    const deletedUser = await this.userSchema.findByIdAndDelete(userId);
    if(deletedUser == null){
      throw new HttpException(409, 'Id not valid');
    }
    return deletedUser
  }

  private createToken(user: IUser): TokenData{
    const dataInToken: DataStoreInToken = {id: user._id};
    const secret: string = process.env.JWT_TOKEN_SECRET!;
    const expiresIn: number = 60;
    return {
      token : jwt.sign(dataInToken, secret, {expiresIn: expiresIn})
    }
  }
}

export default UserService;