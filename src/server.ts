import 'dotenv/config'

import { IndexRoute } from './modules/index';
import App from './app'
import { validateEnv } from './core/utils';
import UsersRoute from '@modules/users/user.route';
import AuthRoute from '@modules/auth/auth.route';
import { ProfileRoute } from '@modules/profile';
import TheatersRoute from '@modules/theater/theater.route';
import { TicketsRoute } from '@modules/ticket';

validateEnv()

const routes = [
  new IndexRoute(),
  new UsersRoute(),
  new AuthRoute(),
  new ProfileRoute(),
  new TheatersRoute(),
  new TicketsRoute
];

const app = new App(routes);

app.listen();